package com.paad.helloworld;

import android.app.Activity;
import android.os.Bundle;
import android.widget.TextView;

public class MyActivity extends Activity {

    /** 第一次创建Activity时被调用*/
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.main);

        TextView myTextView = (TextView) findViewById(R.id.myTextView);
    }
}
